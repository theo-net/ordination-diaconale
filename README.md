Ordination Diaconale
======

## Installation de l'application avec Docker (recommandé)

[Consulter la documentation technique](docs/INSTALL.md)

Les commandes PHP et composer devront être préfixées par : 

        bin/tools

Les commandes Js (NodeJs, yarn ...) seront préfixées par :

        bin/node-tools

## Installation sur le serveur

Exécuter les commandes suivantes, après avoir récupéré les sources : 

        composer install --no-dev --optimize-autoloader
        composer dump-autoload --optimize --apcu --classmap-authoritative --no-dev
        composer dump-env prod
        yarn install --pure-lockfile
        php bin/console cache:clear
        yarn encore production

