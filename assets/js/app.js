import '@fortawesome/fontawesome-free/css/all.min.css';

(() => {
  let mainNavbar = document.querySelector('#main-navbar');
  let mainNavbarButton = document.querySelector('#main-navbar > button');

  if (mainNavbarButton) {
    mainNavbarButton.addEventListener(
      'click',
      (e) => {
        mainNavbarButton.classList.toggle('is-active');
        mainNavbar.classList.toggle('is-opened');
        e.preventDefault();
      },
      false,
    );
  }

  let selectLangMenu = document.querySelector('#select-lang-menu');
  let selectLangMenuButton = document.querySelector('#select-lang-menu > a');
  let selectLangMenuButtonArrow = document.querySelector('#select-lang-menu > a > #lang-arrow');

  if (selectLangMenuButton) {
    selectLangMenuButton.addEventListener(
      'click',
      (e) => {
        selectLangMenuButtonArrow.classList.toggle('fa-arrow-down');
        selectLangMenuButtonArrow.classList.toggle('fa-arrow-up');
        selectLangMenu.classList.toggle('is-opened');
        e.preventDefault();
      },
      false,
    );
  }

  /**
   * Countdown
   */
  const MINUTES = 60;
  const HOURS = 60 * MINUTES;
  const DAYS = 24 * HOURS;

  const elements = {
    days: document.querySelector('#countdown-ordination .countdown-days span'),
    hours: document.querySelector('#countdown-ordination .countdown-hours span'),
    minutes: document.querySelector('#countdown-ordination .countdown-minutes span'),
    seconds: document.querySelector('#countdown-ordination .countdown-seconds span'),
  };
  let previousDiff = {};
  const countdown = document.querySelector('#countdown-ordination');
  const countdownBlock = document.querySelector('.countdown');
  const launchDate = Date.parse(countdown.dataset.time) / 1000;

  function refreshCountdown() {
    const difference = launchDate - Date.now() / 1000;
    if (difference <= 0) {
      updateDom({ days: 0, hours: 0, minutes: 0, seconds: 0 });
      return;
    }
    const diff = {
      days: Math.floor(difference / DAYS),
      hours: Math.floor((difference % DAYS) / HOURS),
      minutes: Math.floor((difference % HOURS) / MINUTES),
      seconds: Math.floor(difference % MINUTES),
    };

    updateDom(diff);

    window.setTimeout(() => {
      window.requestAnimationFrame(refreshCountdown);
    }, 1000);
  }

  function updateDom(diff) {
    let end = 0;
    Object.keys(diff).forEach((key) => {
      end += diff[key];
      if (previousDiff[key] !== diff[key]) {
        elements[key].innerText = diff[key];
      }
    });

    if (0 === end) {
      countdownBlock.classList.add('hidden');
    }

    previousDiff = diff;
  }

  refreshCountdown();
})();
